const NodeCache = require('node-cache');

const ENVIRONMENT = process.env.NODE_ENV;

const nodeCache = new NodeCache();

/**
 *
 * @param {string} key Key of the cache
 * @returns {any} Value of the cache
 */
const getMemCache = (key) => {
  if (ENVIRONMENT === 'test' || ENVIRONMENT === 'testing') return undefined;

  return nodeCache.get(key);
};

/**
 *
 * @param {string} key Key to set
 * @param {object} valueObj Object to store in cache
 * @param {number}  [timeInSeconds=10000]  TTL in second, after this time return undefined
 * @returns {boolean} status of the creation
 */
const setMemCache = (key, valueObj, timeInSeconds = 10000) => {
  if (ENVIRONMENT === 'test' || ENVIRONMENT === 'testing') return undefined;

  return nodeCache.set(key, valueObj, timeInSeconds);
};
/**
 *
 * @param {string} key Key to remove
 * @returns {number} — Number of deleted keys
 */
const removeMemCache = (key) => {
  if (ENVIRONMENT === 'test' || ENVIRONMENT === 'testing') return undefined;

  return nodeCache.del(key);
};

module.exports = {
  setMemCache,
  getMemCache,
  removeMemCache,
};
