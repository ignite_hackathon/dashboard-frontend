const fetch = require("node-fetch");
const jwt = require("jsonwebtoken");

const { getMemCache, setMemCache} = require("../libs/cache/cache");
const { CACHE_KEYS } = require("../libs/cache/config");
const { OAUTH_URL, CLIENT_ID, CLIENT_SECRET, AUDIENCE, GRANT_TYPE, COOKIE } = require("./config.js");

const bodyOAuth = {
    "client_id": CLIENT_ID,
    "client_secret": CLIENT_SECRET,
    "audience": AUDIENCE,
    "grant_type": GRANT_TYPE
}

const getM2MToken = async () => {
    const accessToken = getMemCache(CACHE_KEYS.M2M_TOKEN)

    if (accessToken) return accessToken

    return fetch(`${OAUTH_URL}/oauth/token`, {
        method: 'POST',
        body: JSON.stringify(bodyOAuth),
        headers: {
            'Content-Type': 'application/json',
            'Cookie': COOKIE
        }
    })
        .then(res => res.json())
        .then(tokenData => {
            const { exp: expiredIn } = jwt.decode(tokenData.access_token);
            const tokenResponse = { accessToken: `Bearer ${tokenData.access_token}`, expiredIn }

            setMemCache(
                CACHE_KEYS.M2M_TOKEN,
                tokenResponse,
                parseInt(parseInt(expiredIn, 10) / 2, 10)
            );

            return tokenResponse
        })
}

module.exports = {
    getM2MToken
}