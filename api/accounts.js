const express = require("express");
const requireAuth = require("./_require-auth");
const { getAccountsByOwner, getAccountByOwner } = require("./_db");
const router = express.Router();

router.get("/", requireAuth, async (req, res) => {
  const authUser = req.user;
  const { owner } = req.query;

  // Make sure owner is authenticated user
  if (owner !== authUser.uid) {
    return res.send({
      status: "error",
      message: "Cannot get accounts that belong to a different owner",
    });
  }

  const accounts = await getAccountsByOwner(owner);

  res.send({
    status: "success",
    data: accounts,
  });
});

router.get("/:accountId", requireAuth, async (req, res) => {
  const authUser = req.user;
  const { owner } = req.query;
  const { accountId } = req.params;

  // Make sure owner is authenticated user
  if (owner !== authUser.uid) {
    return res.send({
      status: "error",
      message: "Cannot get account transactions that belong to a different owner",
    });
  }

  const account = await getAccountByOwner(owner, accountId);

  res.send({
    status: "success",
    data: account,
  });
});

module.exports = router;
