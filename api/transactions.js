const express = require("express");
const requireAuth = require("./_require-auth.js");
const { getTransactionsByOwner } = require("./_db.js");
const {getClassifiedTransactionsByOwner} = require("./_db");
const router = express.Router();

router.get("/", requireAuth, async (req, res) => {
  const authUser = req.user;
  const { owner } = req.query;

  // Make sure owner is authenticated user
  if (owner !== authUser.uid) {
    return res.send({
      status: "error",
      message: "Cannot get items that belong to a different owner",
    });
  }

  const transactions = await getTransactionsByOwner(owner);

  res.send({
    status: "success",
    data: transactions,
  });
});

router.get("/classified", requireAuth, async (req, res) => {
  const authUser = req.user;
  const { owner } = req.query;

  // Make sure owner is authenticated user
  if (owner !== authUser.uid) {
    return res.send({
      status: "error",
      message: "Cannot get items that belong to a different owner",
    });
  }

  const classifiedTransactions = await getClassifiedTransactionsByOwner(owner);

  res.send({
    status: "success",
    data: classifiedTransactions,
  })
})

module.exports = router;
