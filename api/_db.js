const fetch = require("node-fetch");
const {Headers} = require("node-fetch");


// Prisma Client
const prisma = require("./_prisma-client");
const {getM2MToken} = require("./auth/authM2M");

const CORE_SERVICE = "core-service";

const BASE_PATH = process.env.NODE_ENV === "development"
    ? `https://${CORE_SERVICE}.nubemotic.com`
    : `http://${CORE_SERVICE}`;

/**** USERS ****/

// Get user by uid
async function getUser(uid) {
  return prisma.user.findUnique({
    where: {
      id: uid,
    }
  });
}

// Create a new user
async function createUser(uid, data) {
  const user = await getUser(uid);

  if (user) {
    return user;
  }

  return prisma.user.create({
    data: {
      id: uid,
      ...data,
    },
  })
}

// Update an existing user
function updateUser(uid, data) {
  return prisma.user.update({
    where: {
      id: uid,
    },
    data: { ...data },
  })
}


const privateRequest = async (url) => {
  const { accessToken } = await getM2MToken()

  const meta = {
    'Authorization': accessToken
  };
  const headers = new Headers(meta);

  const r = await fetch(url, {headers})
  return r.json();
}

/**** PROVIDERS ****/

// Fetch all accounts by owner
async function getProviders(owner) {
  return privateRequest(`${BASE_PATH}/providers`)
}

/**** ACCOUNTS ****/

// Fetch all accounts by owner
async function getAccountsByOwner(owner) {
  return privateRequest(`${BASE_PATH}/accounts?owner=${owner}`)
}

// Fetch account transaction by owner and account id
async function getAccountByOwner(owner, accountId) {
  return privateRequest(`${BASE_PATH}/accounts/${accountId}?owner=${owner}`)
}

/**** TRANSACTIONS ****/

// Fetch all transactions by owner
async function getTransactionsByOwner(owner) {
  return privateRequest(`${BASE_PATH}/movements?owner=${owner}`)
}

// Fetch all transactions by owner
async function getClassifiedTransactionsByOwner(owner) {
  return privateRequest(`${BASE_PATH}/classifier/movements?owner=${owner}`)
}

module.exports = {
  getUser,
  createUser,
  updateUser,

  getProviders,

  getAccountsByOwner,
  getAccountByOwner,

  getTransactionsByOwner,
  getClassifiedTransactionsByOwner,
};
