require("dotenv").config();
const express = require("express");
const authUser = require("./auth-user.js");
const user = require("./user.js");
const accounts = require("./accounts.js");
const transactions = require("./transactions.js");
const path = require("path");

const isDevelopment = process.env.NODE_ENV === 'development';

const PORT = 8080;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/auth-user", authUser);
app.use("/api/user", user);
app.use("/api/accounts", accounts);
app.use("/api/transactions", transactions);

app.listen(PORT, function () {
  console.log(`Server listening on port ${PORT}`);
});

const root = require("path").join(__dirname, "build");
app.use(express.static(root));

!isDevelopment && app.use(reactAppMiddleware); //Serves resources from public folder

function reactAppMiddleware(req, res, next) {
  if (req.xhr || req.headers.accept.indexOf('json') > -1) {
    // if it is an ajax/xhr request, you can even right your custom logic, maybe look for some header
     next();
  } else {
     // Return react app
     res.sendFile(path.join(__dirname, 'build', 'index.html'));
  }
}

