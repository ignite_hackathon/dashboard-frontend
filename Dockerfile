# Install dependencies only when needed
FROM node:alpine AS builder
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
ENV NODE_OPTIONS=--openssl-legacy-provider
COPY . .
# safe data :
ENV AUTH0_CLIENT_ID=f7HuLCo1rGKjhuEbQRGJZWUJAC635KUs
ENV AUTH0_DOMAIN=ignite-hackathon.eu.auth0.com
ENV AUTH0_MANAGEMENT_CLIENT_ID=aR9xtDWF4tKAIM1WC72LYj7X22xHcVqg
ENV REACT_APP_AIRTABLE_NAME=Contact
ENV REACT_APP_AUTH0_CLIENT_ID=f7HuLCo1rGKjhuEbQRGJZWUJAC635KUs
ENV REACT_APP_AUTH0_DOMAIN=ignite-hackathon.eu.auth0.com
#
RUN npm run build

# Production image, copy all the files and run next
FROM node:alpine AS runner
WORKDIR /app

ENV NODE_ENV production

RUN addgroup -g 1001 -S nodejs
RUN adduser -S ignite -u 1001

# You only need to copy next.config.js if you are NOT using the default configuration
# COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/api ./api
COPY --from=builder /app/build ./api/build
COPY --from=builder /app/prisma ./prisma
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json

RUN npx prisma generate

USER ignite

CMD node api
