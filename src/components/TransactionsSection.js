import React from "react";
import Switch from "@material-ui/core/Switch";
import Container from "@material-ui/core/Container";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Section from "./Section";
import SectionHeader from "./SectionHeader";
import TransactionsTable from "./Table/TransactionsTable";
import CategorizedTable from "./Table/CategorizedTable";

function TransactionsSection(props) {
  const [categorizedView, setCategorizedView] = React.useState(false);

  const handleChangeCategorized = (event) => {
    setCategorizedView(event.target.checked);
  };

  return (
    <Section
      bgColor={props.bgColor}
      size={props.size}
      bgImage={props.bgImage}
      bgImageOpacity={props.bgImageOpacity}
    >
      <Container>
        <SectionHeader
          title={props.title}
          subtitle={props.subtitle}
          size={4}
          textAlign="center"
        />

        <FormControlLabel
          control={<Switch checked={categorizedView} onChange={handleChangeCategorized} />}
          label="Categorizar transacciones"
        />

        {categorizedView ? <CategorizedTable /> : <TransactionsTable />}
      </Container>
    </Section>
  );
}

export default TransactionsSection;
