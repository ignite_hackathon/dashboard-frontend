import React from 'react';
import {
  TrendingUp,
  TrendingDown,
} from '@material-ui/icons';
import EnhancedTable from "./EnhancedTable";
import {useCategorizedTransactionsByOwner} from "../../util/db";
import {useAuth} from "../../util/auth";
import CategoriesTabs from "./CategoriesTabs";
import CategoryTab from "./CategoryTab";

const headCells = [
  { id: 'id', numeric: false, disablePadding: true, label: 'ID' },
  { id: 'categories', numeric: false, disablePadding: false, label: 'Categorias' },
  { id: 'date', numeric: false, disablePadding: false, label: 'Fecha' },
  { id: 'detail', numeric: false, disablePadding: false, label: 'Detalle', width: '360px' },
  { id: 'credit', numeric: true, disablePadding: false, label: 'Credito' },
  { id: 'debit', numeric: true, disablePadding: false, label: 'Debito' },
];

const tabs = [
  { label: 'Ingresos', icon: <TrendingUp /> },
  { label: 'Egresos', icon: <TrendingDown /> },
];

export default function CategorizedTable(props) {
  const auth = useAuth();

  // Fetch data by owner
  // Returned status value will be "idle" if we're waiting on
  // the uid value or "loading" if the query is executing.
  const uid = auth.user ? auth.user.uid : undefined;

  const { data, status } = useCategorizedTransactionsByOwner(uid);

  const [selectedTab, setSelectedTab] = React.useState(0);

  const handleChange = (event, newValue) => {
    setSelectedTab(newValue);
  };

  return (
    (status === "idle" || status === "loading") ? (
      <span>Cargando movimientos...</span>
    ) : data.length > 0 && (
      <>
        <CategoriesTabs
          value={selectedTab}
          handleChange={handleChange}
          tabs={tabs}
        />
        <CategoryTab index={0} value={selectedTab}>
          <EnhancedTable rows={data[0].movement} headCells={headCells} />
        </CategoryTab>
        <CategoryTab index={1} value={selectedTab}>
          <EnhancedTable rows={data[1].movement} headCells={headCells} />
        </CategoryTab>
      </>
    )
  );
}
