import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { IconButton, lighten, makeStyles, Switch, Toolbar, Tooltip, Typography, Grid, Divider } from "@material-ui/core";
import { FilterList, ViewHeadline, ViewStream, Warning } from "@material-ui/icons";

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title: {
    flex: '5 1 auto',
  },
  tools: {
    flex: '1 1 auto',
    justifyContent: 'flex-end',
  },
  viewModeToggle: {
    alignItems: 'center',
    width: 'auto',
  }
}));

export default function EnhancedTableToolbar(props) {
  const classes = useToolbarStyles();
  const { numSelected, dense, handleChangeDense } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
          {numSelected} seleccionadas
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
          Movimientos
        </Typography>
      )}

      <Grid container className={classes.tools}>
        <Grid container className={classes.viewModeToggle}>
          <Grid flexItem>
            <IconButton disable aria-label="reportar">
              <ViewStream />
            </IconButton>
          </Grid>
          <Grid flexItem>
            <Switch checked={dense} onChange={handleChangeDense} />
          </Grid>
          <Grid flexItem>
            <IconButton disable aria-label="reportar">
              <ViewHeadline />
            </IconButton>
          </Grid>
        </Grid>
        <Divider orientation="vertical" flexItem />
        <Grid flexItem>
          {numSelected > 0 ? (
            <Tooltip title="Reportar">
              <IconButton aria-label="reportar">
                <Warning />
              </IconButton>
            </Tooltip>
          ) : (
            <Tooltip title="Filtrar">
              <IconButton aria-label="filtrar">
                <FilterList />
              </IconButton>
            </Tooltip>
          )}
        </Grid>
      </Grid>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};
