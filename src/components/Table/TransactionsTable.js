import React from 'react';
import EnhancedTable from "./EnhancedTable";
import {useTransactionsByOwner} from "../../util/db";
import {useAuth} from "../../util/auth";

const headCells = [
  { id: 'id', numeric: false, disablePadding: true, label: 'ID' },
  { id: 'date', numeric: false, disablePadding: false, label: 'Fecha' },
  { id: 'detail', numeric: false, disablePadding: false, label: 'Detalle', width: '360px' },
  { id: 'credit', numeric: true, disablePadding: false, label: 'Credito' },
  { id: 'debit', numeric: true, disablePadding: false, label: 'Debito' },
];

export default function TransactionsTable(props) {
  const auth = useAuth();

  // Fetch data by owner
  // Returned status value will be "idle" if we're waiting on
  // the uid value or "loading" if the query is executing.
  const uid = auth.user ? auth.user.uid : undefined;

  const { data, status } = useTransactionsByOwner(uid);

  return (
    ((status === "idle" || status === "loading")) ? (
      <span>Cargando movimientos...</span>
    ) : data.length > 0 && (
      <EnhancedTable rows={data[0].movement} headCells={headCells} />
    )
  );
}
