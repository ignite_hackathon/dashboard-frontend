import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import PriceCell from "./PriceCell";
import {Chip} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  detail: {
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
    display: "block",
    width: "360px",
  },
  categories: {
    maxWidth: "60px"
  }
}));

export default function TransactionRow(props) {
  const {
    isItemSelected,
    labelId,
    row: {
      id,
      detail,
      date,
      debit,
      credit,
      categories,
    },
    handleClick,
  } = props;

  const classes = useStyles();

  return (
    <TableRow
      hover
      onClick={(event) => handleClick(event, id)}
      role="checkbox"
      aria-checked={isItemSelected}
      tabIndex={-1}
      key={id}
      selected={isItemSelected}
    >
      <TableCell padding="checkbox">
        <Checkbox
          checked={isItemSelected}
          inputProps={{'aria-labelledby': labelId}}
        />
      </TableCell>
      <TableCell id={labelId} scope="row" padding="none">
        <Chip label={id.slice(-4)} variant="outlined" size="small" />
      </TableCell>
      {categories && (
        <TableCell className={classes.categories} scope="row" padding="none">
          {categories.map(category => (
            <Chip label={category} variant="outlined" size="small" />
          ))}
        </TableCell>
      )}
      <TableCell align="left">{date}</TableCell>
      <TableCell className={classes.detail} align="left">{detail}</TableCell>
      <PriceCell type='credit' amount={credit} />
      <PriceCell type='debit' amount={debit} />
    </TableRow>
  );
}
