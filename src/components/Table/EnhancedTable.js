import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
  Paper,
  makeStyles,
  Grid,
} from '@material-ui/core';
import TransactionRow from "./TransactionRow";
import EnhancedTableToolbar from "./EnhancedTableToolbar";
import EnhancedTableHead from "./EnhancedTableHead";
import CategoriesTabs from "./CategoriesTabs";
import EnhancedTableContainer from "./EnhancedTableContainer";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTable(props) {
  const { rows = [], headCells = [] } = props;

  const classes = useStyles();

  const [selected, setSelected] = React.useState([]);

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <EnhancedTableContainer
          rows={rows}
          headCells={headCells}
          selected={selected}
          setSelected={setSelected}
        />
      </Paper>
    </div>
  );
}
