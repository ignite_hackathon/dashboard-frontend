import React from 'react';
import {
  Tab,
  Tabs,
  makeStyles,
} from '@material-ui/core';

function a11yProps(label) {
  return {
    id: `scrollable-force-tab-${label}`,
    'aria-controls': `scrollable-force-tabpanel-${label}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
  },
}));

export default function CategoriesTabs(props) {
  const { tabs, value, handleChange } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Tabs
        value={value}
        onChange={handleChange}
        orientation="horizontal"
        variant="scrollable"
        scrollButtons="on"
        indicatorColor="primary"
        textColor="primary"
        aria-label="scrollable force tabs example"
        className={classes.tabs}
      >
        {tabs.length > 0 && tabs.map(tab => (
          <Tab label={tab.label} icon={tab.icon} {...a11yProps(tab.label)} />
        ))}
      </Tabs>
    </div>
  );
}
