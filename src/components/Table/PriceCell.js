import React from "react";
import TableCell from "@material-ui/core/TableCell";
import {Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const priceTypes = {
  DEBIT: 'debit',
  CREDIT: 'credit'
}

const useStyles = makeStyles((theme) => ({
  container: {
    justifyContent: 'space-around',
  },
  item: {
  }
}));

export default function PriceCell(props) {
  const {
    type,
    amount,
  } = props;

  const classes = useStyles();

  const align = amount > 0 ? 'right' : 'center';
  const amountSign = type === priceTypes.DEBIT ? '-' : ' ';

  return (
    <TableCell align={align}>
      <Grid container className={classes.container}>
        <Grid flexItem className={classes.item}>
          {amount > 0 && `$ ${amountSign}`}
        </Grid>
        <Grid flexItem className={classes.item}>
          {amount > 0
            ? amount
            : '-'}
        </Grid>
      </Grid>
    </TableCell>
  )
}
