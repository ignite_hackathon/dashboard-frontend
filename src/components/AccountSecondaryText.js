import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  inline: {
    display: 'inline',
  },
}));

export default function AccountSecondaryText(props) {
  const classes = useStyles();

  const {
    balance,
    currency,
    name,
  } = props;

  return (
    <React.Fragment>
      <Typography
        component="span"
        variant="body2"
        className={classes.inline}
        color="textPrimary"
      >
        {name}
      </Typography>
      {` - Balance ${currency} ${balance}`}
    </React.Fragment>
  );
};
