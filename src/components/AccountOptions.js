import React from "react";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import LinkMui from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import NotificationsIcon from "@material-ui/icons/Notifications";
import AccountPrimaryText from "./AccountPrimaryText";
import AccountSecondaryText from "./AccountSecondaryText";
import { Link } from "../util/router";

const useStyles = makeStyles((theme) => ({
  cardContent: {
    padding: theme.spacing(3),
  },
  button: {
    margin: theme.spacing(1),
  },
}));

export default function AccountOptions(props) {
  const classes = useStyles();

  const { account = undefined } = props;

  const {
    number,
    branch,
    balance,
    currency,
    name,
    id,
  } = account;

  return account && (
    <Card>
      <CardHeader
        title={
          <AccountPrimaryText
            number={number}
            branch={branch}
          />
        }
        subheader={
          <AccountSecondaryText
            balance={balance}
            currency={currency}
            name={name}
          />
        }/>
      <CardContent className={classes.cardContent}>
      </CardContent>
      <CardActions>
        <Button
          variant="outlined"
          size="medium"
          className={classes.button}
          startIcon={<AccountBalanceIcon />}
        >
          <LinkMui
            underline="none"
            color="textPrimary"
            component={Link}
            to={`/accounts/${id}`}
          >
            Ver movimientos
          </LinkMui>
        </Button>
        <Button
          variant="outlined"
          size="medium"
          className={classes.button}
          startIcon={<NotificationsIcon />}
        >
          <LinkMui
            underline="none"
            color="textPrimary"
            component={Link}
            to={`/alerts/${id}`}
          >
            Configurar notificaciones
          </LinkMui>
        </Button>
      </CardActions>
    </Card>
  );
}
