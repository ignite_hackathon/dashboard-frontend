import React from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Section from "./Section";
import SectionHeader from "./SectionHeader";

import ConfigAlerts from "./ConfigAlerts";
import NewAlert from "./NewAlert";
import AlertHistory from "./AlertHistory";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        flexDirection: "row",
        justifyContent: "space-evenly"
    },
    panel: {
        flexDirection: "column",
        justifyContent: "space-evenly"
    },
    panelBox: {
        marginTop: "20px",
        marginRight: "20px"
    }
}));

function AlertsSection(props) {
    const classes = useStyles()

    return (
        <Section
            bgColor={props.bgColor}
            size={props.size}
            bgImage={props.bgImage}
            bgImageOpacity={props.bgImageOpacity}
        >
            <Container>
                <SectionHeader
                    title={props.title}
                    subtitle={props.subtitle}
                    size={4}
                    textAlign="center"
                />
                <Grid className={classes.root} container={true} direction={"row"}>
                    <Grid className={classes.panel} item >
                        <Grid className={classes.panelBox} item={true}  >
                            <ConfigAlerts/>
                        </Grid>
                        <Grid className={classes.panelBox} item={true}>
                            <AlertHistory/>
                        </Grid>
                    </Grid>

                    <Grid className={[classes.panelBox, classes.panel]} item={true} xs={12} md={6}>
                        <NewAlert/>
                    </Grid>

                </Grid>
            </Container>
        </Section>
    );
}

export default AlertsSection;
