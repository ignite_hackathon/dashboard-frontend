import React from "react";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Section from "./Section";
import SectionHeader from "./SectionHeader";

function ClientsSection(props) {
  const items = [
    {
      name: "Prometeo",
      image: "https://d1bvpc91pjhu8a.cloudfront.net/static/img/primary%402x.png",
      width: "150px",
    },
    {
      name: "Open Banking",
      image: "https://www.openbanking.org.uk/wp-content/uploads/2021/03/open-banking-logo.svg",
      width: "320px",
    },
    {
      name: "Telegram",
      image: "https://pluspng.com/img-png/telegram-logo-vector-png-telegram-logo-dallas-cowboys-logo-png-972.png",
      width: "150px",
    },
  ];

  return (
    <Section
      bgColor={props.bgColor}
      size={props.size}
      bgImage={props.bgImage}
      bgImageOpacity={props.bgImageOpacity}
    >
      <Container>
        <Box textAlign="center">
          <SectionHeader
            title={props.title}
            subtitle={props.subtitle}
            size={4}
            textAlign="center"
          />
          <Grid container={true} justifyContent="center">
            {items.map((item, index) => (
              <Grid item={true} xs={12} md="auto" key={index}>
                <Box py={2} px={3}>
                  <img src={item.image} width={item.width} alt={item.name} />
                </Box>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Container>
    </Section>
  );
}

export default ClientsSection;
