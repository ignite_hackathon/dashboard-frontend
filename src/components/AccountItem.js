import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import AccountPrimaryText from "./AccountPrimaryText";
import AccountSecondaryText from "./AccountSecondaryText";

export default function AccountItem(props) {
  const {
    account = {},
    index,
    totalAccounts,
    selected,
    onClick,
  } = props;

  const {
    balance,
    branch,
    currency,
    name,
    number,
  } = account;

  return (
    <ListItem
      key={index}
      divider={index !== totalAccounts - 1}
      selected={selected}
      onClick={() => onClick(account, index)}
    >
      <ListItemText
        primary={
          <AccountPrimaryText
            number={number}
            branch={branch}
          />
        }
        secondary={
          <AccountSecondaryText
            balance={balance}
            currency={currency}
            name={name}
          />
        }
      />
    </ListItem>
  );
};
