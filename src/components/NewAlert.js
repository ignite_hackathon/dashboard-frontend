import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import {CardActions, Checkbox, FormControlLabel, FormGroup, TextField} from "@material-ui/core";
import BasicSelect from "./Select";
import Button from "@material-ui/core/Button";
import {Link} from "../util/router";
import Card from "@material-ui/core/Card";
import React from "react";
import CardHeader from "@material-ui/core/CardHeader";
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between"
    },
    content: {
        flex: "1"
    }
}));

const NewAlert = () => {
    const classes = useStyles()

    return <Card className={classes.root}>
        <CardHeader title="Nueva alerta"/>
        <CardContent className={classes.content}>
            <FormGroup spacing={4} sx={{ p: 2 }}>
                <Box m={2} display="flex" flexDirection={"column"}>
                    <FormControlLabel control={<Checkbox color="primary"/>} label="Telegram"/>
                    <FormControlLabel disabled control={<Checkbox color="primary"/>} label="WhatsApp"/>
                    <FormControlLabel disabled control={<Checkbox color="primary"/>} label="SMS"/>
                </Box>
                    <BasicSelect/>
                <TextField id="outlined-basic" label="Mensaje" variant="outlined" />
            </FormGroup>
        </CardContent>
        <CardActions style={{justifyContent: "flex-end"}}>
            <Button
                component={Link}
                variant="contained"
                size="large"
                color="primary"
            >
                Crear Alerta
            </Button>
        </CardActions>
    </Card>
}

export default NewAlert