import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  inline: {
    display: 'inline',
  },
}));

export default function AccountPrimaryText(props) {
  const classes = useStyles();

  const {
    number,
    branch,
  } = props;

  return (
    <React.Fragment>
      {`${number} `}
      <Typography
        component="span"
        variant="body1"
        className={classes.inline}
        color="textSecondary"
      >
        - Sucursal {branch}
      </Typography>
    </React.Fragment>
  );
};
