import React from "react";
import Box from "@material-ui/core/Box";
import List from "@material-ui/core/List";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";
import { Alert } from "@material-ui/lab";
import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";
import AccountItem from "./AccountItem";
import ConnectNewAccountModal from "./ConnectNewAccountModal";
import { useAuth } from "./../util/auth";
import { useAccountsByOwner } from "./../util/db";

const useStyles = makeStyles(() => ({
  paperItems: {
    minHeight: "300px",
  },
}));

function DashboardAccounts(props) {
  const classes = useStyles();

  const auth = useAuth();

  const [selectedAccount, setSelectedAccount] = React.useState(-1);

  const { onAccountSelection } = props;

  const {
    data: { accounts } = {},
    status: accountsStatus,
    error: accountsError,
  } = useAccountsByOwner(auth.user.uid);

  const [connectingAccount, setConnectingAccount] = React.useState(false);

  const accountsAreEmpty = !accounts || accounts.length === 0;

  const handleAccountSelection = (account, index) => {
    if (selectedAccount === index) {
      setSelectedAccount(-1);
      onAccountSelection({});
    } else {
      setSelectedAccount(index);
      onAccountSelection(account);
    }
  };

  return (
    <>
      {accountsError && (
        <Box mb={3}>
          <Alert severity="error">{accountsError.message}</Alert>
        </Box>
      )}

      <Paper className={classes.paperItems}>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          padding={2}
        >
          <Typography variant="h5">Cuentas</Typography>
          <Button
            variant="contained"
            size="medium"
            color="primary"
            onClick={() => setConnectingAccount(true)}
            endIcon={<PlaylistAddIcon />}
          >
            Conectar nueva cuenta
          </Button>
        </Box>
        <Divider />

        {(accountsStatus === "loading" || accountsAreEmpty) && (
          <Box py={5} px={3} align="center">
            {accountsStatus === "loading" && <CircularProgress size={32} />}

            {accountsStatus !== "loading" && accountsAreEmpty && (
              <>No tienes cuentas vinculadas. Agrega una nueva cuenta desde el botón de arriba.</>
            )}
          </Box>
        )}

        {accountsStatus !== "loading" && accounts && accounts.length > 0 && (
          <List disablePadding={true}>
            {accounts.map((account, index) => (
              <AccountItem
                account={account}
                index={index}
                selected={selectedAccount === index}
                totalAccounts={accounts.length}
                onClick={handleAccountSelection}
              />
            ))}
          </List>
        )}
      </Paper>

      {connectingAccount && <ConnectNewAccountModal onDone={() => setConnectingAccount(false)} />}
    </>
  );
}

export default DashboardAccounts;
