import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import LinkMui from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "./../util/router";

const useStyles = makeStyles((theme) => ({
  cardContent: {
    padding: theme.spacing(3),
  },
}));

export default function AccountInstructions(props) {
  const classes = useStyles();

  const { auth } = props;

  return (
    <Card>
      <CardContent className={classes.cardContent}>
        <Box>
          <Typography variant="h6" paragraph={true}>
            <strong>Qué puedo hacer acá</strong>
          </Typography>
          <Typography paragraph={true}>
            Este es tu dashboard, donde veras las entidades bancarias con
            las que sincronizaste tu cuenta.
          </Typography>
          <Typography paragraph={true}>
            Podes agregar nuevas entidades bancarias, seleccionar alguna
            de las entidades y ver los movimientos asociados o configurar
            alertas relacionadas.
          </Typography>
          <Box mt={3}>
            <Typography variant="h6" paragraph={true}>
              <strong>Información útil</strong>
            </Typography>
            <Typography component="div">
              <div>
                Estás logueado con tu correo <strong>{auth.user.email}</strong>.
              </div>

              <div>
                Puedes modificar los datos de tu cuenta en{` `}
                <LinkMui component={Link} to="/settings/general">
                  <strong>configuración</strong>
                </LinkMui>
                .
              </div>
            </Typography>
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
}
