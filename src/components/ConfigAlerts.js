import * as React from 'react';
import {Paper} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import {Alert} from "@material-ui/lab";
import {Star} from "@material-ui/icons";
import Typography from "@material-ui/core/Typography";


export default function ConfigAlerts() {
    return (
        <Paper>
            <Box padding={2}>
                <Typography variant="h5">Alertas configuradas</Typography>
                <Alert icon={<Star/>} variant="outlined" style={{margin: '1rem'}} severity="info">
                    Cantidad de operaciones </Alert>
                <Alert icon={<Star/>} variant="outlined" style={{margin: '1rem'}} severity="info">
                    Monto fuera de rango
                </Alert>
                <Alert icon={<Star/>} variant="outlined" style={{margin: '1rem'}} severity="info">
                    Se realizaron mas transacciones de lo habitual
                </Alert>
            </Box>
            <Divider/>
        </Paper>
    );
}


