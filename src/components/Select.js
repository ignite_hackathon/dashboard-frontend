import * as React from 'react';
import Box from "@material-ui/core/Box";
import {FormControl, FormHelperText, InputLabel, MenuItem, Select} from "@material-ui/core";


export default function BasicSelect() {
    const [age, setAge] = React.useState(10);

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return (
        <FormControl sx={{ m: 1, minWidth: 120 }}>
            <Select
                value={age}
                onChange={handleChange}
                displayEmpty
                inputProps={{ 'aria-label': 'Without label' }}
                variant="outlined"
            >
                <MenuItem value="">
                    <em>Mensaje por defecto</em>
                </MenuItem>
                <MenuItem value={10}>Monto fuera de rango</MenuItem>
                <MenuItem value={20}>Cantidad de operaciones</MenuItem>
            </Select>
            <FormHelperText>Te llegara al canal elegido</FormHelperText>
        </FormControl>
    );
}