import * as React from 'react';
import {Alert} from "@material-ui/lab";
import {Paper} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

export default function AlertHistory() {
    return (
        <Paper>
            <Box padding={2}>
                <Typography variant="h5">Historico de alertas</Typography>

                <Alert style={{margin: '1rem'}} severity="info">Se realizaron mas transacciones de lo habitual</Alert>
                <Alert style={{margin: '1rem'}} severity="info">Se supero el monto maximo configurado</Alert>
                <Alert style={{margin: '1rem'}} severity="info"> Se realizaron mas transacciones de lo habitual</Alert>
            </Box>
            <Divider/>
        </Paper>
    );
}