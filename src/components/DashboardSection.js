import React from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Section from "./Section";
import SectionHeader from "./SectionHeader";
import DashboardAccounts from "./DashboardAccounts";
import AccountOptions from "./AccountOptions";
import AccountInstructions from "./AccountInstructions";
import { useAuth } from "./../util/auth";

function DashboardSection(props) {
  const auth = useAuth();

  const [selectedAccount, setSelectedAccount] = React.useState({});

  const [accountSelected, setAccountSelected] = React.useState(false);

  React.useEffect(() => {
    Object.entries(selectedAccount).length === 0
      ? setAccountSelected(false)
      : setAccountSelected(true);
  }, [selectedAccount])

  return (
    <Section
      bgColor={props.bgColor}
      size={props.size}
      bgImage={props.bgImage}
      bgImageOpacity={props.bgImageOpacity}
    >
      <Container>
        <SectionHeader
          title={props.title}
          subtitle={props.subtitle}
          size={4}
          textAlign="center"
        />
        <Grid container={true} spacing={4}>
          <Grid item={true} xs={12} md={6}>
            <DashboardAccounts onAccountSelection={setSelectedAccount}/>
          </Grid>
          <Grid item={true} xs={12} md={6}>
            {accountSelected ? (
              <AccountOptions account={selectedAccount}/>
            ) : (
              <AccountInstructions auth={auth} />
            )}
          </Grid>
        </Grid>
      </Container>
    </Section>
  );
}

export default DashboardSection;
