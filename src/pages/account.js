import React from "react";
import Meta from "../components/Meta";
import TransactionsSection from "../components/TransactionsSection";
import { useRouter } from "./../util/router";
import { requireAuth } from "./../util/auth";

function AccountPage(props) {
  const router = useRouter();

  return (
    <>
      <Meta title="Transacciones" />
      <TransactionsSection
        bgColor="default"
        size="medium"
        bgImage=""
        bgImageOpacity={1}
        title="Transacciones"
        subtitle=""
        account={router.query.accountId}
      />
    </>
  );
}

export default requireAuth(AccountPage);
