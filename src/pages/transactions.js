import React from "react";
import Meta from "./../components/Meta";
import TransactionsSection from "./../components/TransactionsSection";
import { requireAuth } from "./../util/auth";

function TransactionsPage(props) {
  return (
    <>
      <Meta title="Dashboard" />
      <TransactionsSection
        bgColor="default"
        size="medium"
        bgImage=""
        bgImageOpacity={1}
        title="Transactions"
        subtitle=""
      />
    </>
  );
}

export default requireAuth(TransactionsPage);
