import React from "react";
import { requireAuth } from "./../util/auth";
import AlertsSection from "../components/AlertsSection";

function AlertsPage() {
    return (
        <AlertsSection
            bgColor="default"
            size="medium"
            bgImage=""
            bgImageOpacity={1}
            title="Configura tus alertas"
            subtitle=""
        />
    );
}

export default requireAuth(AlertsPage);
